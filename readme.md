# wdays
Really, REALLY simple way to log work hours, storing data in a SQLite file.

```
> 2021-08-16 | 07.00 | Krsand office
> 2021-08-17 | 07.50 | Krsand office
> 2021-08-18 | 07.50 | Krsand office
> 2021-08-19 | 06.00 | 0645-0725, 08-15, some back and forth
> 2021-08-20 | 08.50 | Home office
―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
☝ Total hours for week 33: 36.50
―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
> 2021-08-23 | 08.50 | Home office
> 2021-08-24 | 09.00 | Home office
―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
☝ Total hours for week 34 so far: 17.50
―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
Week average: 34.20 | Total: 171.00 | Counting 23 days
Usage: wdays [yyyy-mm-dd | d for today] [hours]
```

## Usage examples
See logged hours for the last weeks:
```
wdays 
```
Log 7.5 hours for today:
```
wdays d 7.5
```
Log 7.5 hours for yesterday:
```
wdays y 7.5
```
Log 7.5 hours for a specific day (use YYYY-MM-DD):
```
wdays 2021-05-18 7.5
```
See logged hours for a specific day (use YYYY-MM-DD):
```
wdays 2021-05-18
```
Edit logged hours for a specific day (use YYYY-MM-DD):
```
wdays 2021-05-18 8.5
```

## Todo
- ```wdays d``` should create an entry if it doesn't already exist
- New silent flag for no comment (makes it easier to include wdays in other scripts)
- While editing entries hours since entry creation should also be displayed
package main

import (
	"github.com/jinzhu/gorm"
)

type Day struct {
	gorm.Model
	Day     string  `json:"day"`
	Hours   float64 `json:"hours"`
	Comment string  `json:"comment"`
}

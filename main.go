package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"os/exec"
	"sort"
	"strconv"
	"time"

	"github.com/gookit/color"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

func getTermDim() (width, height int, err error) {
	// Found it here:
	// https://stackoverflow.com/questions/50986225/terminal-in-golang-syscall-vs-os-exec-stty
	cmd := exec.Command("stty", "size")
	cmd.Stdin = os.Stdin
	var termDim []byte
	if termDim, err = cmd.Output(); err != nil {
		return
	}
	fmt.Sscan(string(termDim), &height, &width)
	return
}

func dbconn() *gorm.DB {

	cacheDir, _ := os.UserConfigDir()
	err := os.MkdirAll(cacheDir+"/wdays", 0744)
	if err != nil {
		log.Fatal(err)
	}
	//cacheDir, _ := os.UserCacheDir() cacheDir+
	//db, err := gorm.Open("sqlite3", "/Users/haldorsen/wdays/wdays.db")
	db, err := gorm.Open("sqlite3", cacheDir+"/wdays/wdays.db")
	if err != nil {
		fmt.Println(err.Error())
		panic("failed to connect database")
	}

	return db
}

func main() {

	// Connecting to database
	db := dbconn()

	// Migrate the schema
	db.AutoMigrate(&Day{})

	// Filling out variables
	argLength := len(os.Args[1:])

	if argLength == 0 {
		showLatest(365)
		fmt.Println("Usage: wdays [yyyy-mm-dd | d for today] [hours]")
	} else if argLength == 1 {
		inDay := os.Args[1]
		if inDay == "d" {
			now := time.Now()
			inDay = fmt.Sprintf("%04d-%02d-%02d", now.Year(), int(now.Month()), now.Day())
		}
		if inDay == "y" {
			now := time.Now()
			inDay = fmt.Sprintf("%04d-%02d-%02d", now.Year(), int(now.Month()), now.Day()-1)
		}
		showDay(inDay)
	} else if argLength == 2 {
		inDay := os.Args[1]
		if inDay == "d" {
			now := time.Now()
			inDay = fmt.Sprintf("%04d-%02d-%02d", now.Year(), int(now.Month()), now.Day())
		}
		if inDay == "y" {
			now := time.Now()
			inDay = fmt.Sprintf("%04d-%02d-%02d", now.Year(), int(now.Month()), now.Day()-1)
		}
		inHours, _ := strconv.ParseFloat(os.Args[2], 64)
		addDay(inDay, inHours)
	}
}

func showLatest(showLast int) {

	twidth, _, _ := getTermDim()
	line := ""

	for i := 0; i < twidth; i++ {
		line += "―"
	}

	// Connecting to database
	db := dbconn()

	var days []Day
	db.Order("day desc").Limit(showLast).Find(&days)

	var total float64
	var records int
	weekNumber := 0
	weeksTotal := 0
	var weekTot float64

	// Reversing order
	sort.SliceStable(days, func(p, q int) bool {
		return days[p].Day < days[q].Day
	})

	for _, day := range days {

		// Calculating week total
		formatedDay := day.Day + "T00:00:00Z"
		date, _ := time.Parse(time.RFC3339, formatedDay)
		_, week := date.ISOWeek()
		if week != weekNumber {
			if weekNumber != 0 {
				// New week. Print out the old week.
				// Printing out last week
				color.Gray.Println(line)
				color.Printf("<gray>☝ Total hours for</> week %v: %05.2f\n", weekNumber, weekTot)
				color.Gray.Println(line)

			}
			weeksTotal++
			weekTot = day.Hours
			weekNumber = week
		} else {
			weekTot += day.Hours
			week = weekNumber
		}

		color.Printf("<gray>></> %v <gray>|</> %05.2f <gray>| </>", day.Day, day.Hours)
		if day.Comment != "" {
			color.Gray.Printf(day.Comment)
		}
		fmt.Println("")
		total += day.Hours
		records++
	}

	// Printing out last week
	color.Gray.Println(line)
	color.Printf("<gray>☝ Total hours for</> week %v <gray>so far:</> %05.2f\n", weekNumber, weekTot)
	color.Gray.Println(line)

	weekAverage := total / float64(weeksTotal)

	fmt.Printf("Week average: %.2f | Total: %.2f | Counting %v days", weekAverage, total, records)
	fmt.Println("")

	// Closing DB connection
	defer db.Close()
}

func showDay(day string) {

	// Connecting to database
	db := dbconn()

	var oldDay Day
	db.Where("day = ?", day).First(&oldDay)

	if oldDay.Day != "" {
		fmt.Printf("%v | %.2f | %v\n", oldDay.Day, oldDay.Hours, oldDay.Comment)
	} else {
		fmt.Printf("No records for: %v", day)
		fmt.Println("")
	}

	// Closing DB connection
	defer db.Close()
}

func addDay(day string, hours float64) {

	// Connecting to database
	db := dbconn()

	var oldDay Day
	db.Where("day = ?", day).First(&oldDay)

	if oldDay.Day != "" {
		fmt.Printf("Old: %v | %.2f | %v\n", oldDay.Day, oldDay.Hours, oldDay.Comment)
		fmt.Printf("New: %v | %.2f\n", day, hours)
		fmt.Println("Write an optional comment and click enter to overwrite")
	} else {
		fmt.Printf("New: %v | %.2f\n", day, hours)
		fmt.Println("Write an optional comment and click enter to save")
	}

	// Getting comment
	scanner := bufio.NewScanner(os.Stdin)
	var comment string
	for {
		// reads user input until \n by default
		scanner.Scan()
		// Holds the string that was scanned
		comment = scanner.Text()
		if len(comment) != 0 {
			// Not empty string
			break
		} else {
			// exit if user entered an empty string
			break
		}

	}

	// handle error
	if scanner.Err() != nil {
		fmt.Println("Error: ", scanner.Err())
	}

	// Saving to db
	if oldDay.Day != "" {
		oldDay.Day = day
		oldDay.Hours = hours
		if comment != "" {
			oldDay.Comment = comment
		}
		db.Save(&oldDay)
	} else {
		db.Create(&Day{Day: day, Hours: hours, Comment: comment})
	}

	showLatest(50)

	// Closing DB connection
	defer db.Close()

}

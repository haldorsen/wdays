module gitlab.com/haldorsen/wdays

go 1.16

require (
	github.com/gookit/color v1.4.2 // indirect
	github.com/jinzhu/gorm v1.9.16
)
